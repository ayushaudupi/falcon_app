package com.example.myapplication;

public class UnitItem {


        private String text1;
        private String activityname;
        private String activitystatus;
        private String stepname;
        private String progress;

        public UnitItem( String text1) {
            this.text1 = text1;
//            this.text2 = text2;
        }
        public UnitItem( String name, String status, String step_name, String progress) {
            this.activityname = name;
            this.activitystatus = status;
            this.stepname = step_name;
            this.progress = progress;
        }

        public String getText1() {
            return text1;
        }

        public String getStepname() { return stepname; }

        public String getActivityname() { return activityname; }

        public String getActivitystatus() { return activitystatus; }

        public String getProgress() { return progress; }

}
