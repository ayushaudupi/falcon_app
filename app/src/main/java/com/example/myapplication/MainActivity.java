package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.SearchView;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;
import android.view.Menu;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<Object> activities = new ArrayList<>();
    private UnitAdapter adapter;
    private List<UnitItem> exampleList;
    EditText searchinput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        searchinput = (EditText) findViewById(R.id.searchInput) ;

        searchinput.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchinput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
                adapter.getFilter().filter(s);
            }
        });
        get_json();
        setUpRecyclerView();
    }

    public void get_json(){
        String json;

        try {
            InputStream is = getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");
            JSONArray jsonarray = new JSONArray(json);
            exampleList = new ArrayList<>();
            String activity_name, activity_status,step_name;
            String progress;
            for(int i=0;i<=jsonarray.length();i++){
                JSONObject jsonobj = jsonarray.getJSONObject(i);
                JSONArray unitarr = jsonobj.getJSONArray("units");
                for ( int j=0; j < unitarr.length(); j++){
                    JSONObject unitobj = unitarr.getJSONObject(j);
                    JSONArray activitesarr = unitobj.getJSONArray("activities");
                    for (int k=0; k < activitesarr.length(); k++){
                        JSONObject activitiesobj = activitesarr.getJSONObject(k);
                        activity_name = activitiesobj.getString("activity_name");
                        activity_status = activitiesobj.getString("activity_status");
                        step_name = activitiesobj.getString("step_name");
                        progress = activitiesobj.getString("progress");
                        exampleList.add(new UnitItem(activity_name,activity_status,step_name,progress));
                    }
//                    String name = jsonobj.getString("block_name");
//                    exampleList.add(new UnitItem(name));
                }
            }
        }catch(IOException io){
            io.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setUpRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new UnitAdapter(exampleList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu, menu);
//        MenuItem searchItem = menu.findItem(R.id.actionSearch);
//        searchinput.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
////                Toast.makeText(getApplicationContext(),query,Toast.LENGTH_LONG).show();
////                Log.v("QUERY",query);
//
//                return false;
//            }
//            @Override
//            public boolean onQueryTextChange(String newText) {
////                Toast.makeText(getApplicationContext(),newText,Toast.LENGTH_LONG).show();
////                Log.v("search",newText);
//                adapter.getFilter().filter(newText);
//                return false;
//            }
//        });
//        return true;
//    }
}